-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 192.168.137.139    Database: sso-client
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oauth2_authorized_client`
--

DROP TABLE IF EXISTS `oauth2_authorized_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth2_authorized_client` (
  `client_registration_id` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `principal_name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `access_token_type` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `access_token_value` blob NOT NULL,
  `access_token_issued_at` timestamp NOT NULL,
  `access_token_expires_at` timestamp NOT NULL,
  `access_token_scopes` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `refresh_token_value` blob,
  `refresh_token_issued_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_registration_id`,`principal_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_authorized_client`
--

LOCK TABLES `oauth2_authorized_client` WRITE;
/*!40000 ALTER TABLE `oauth2_authorized_client` DISABLE KEYS */;
INSERT INTO `oauth2_authorized_client` VALUES ('messaging-client-authorization-code','user1','Bearer',_binary 'eyJraWQiOiJhNGVmYTc2Yi01YjU1LTQ4ZjgtODk1OC0wYzg1YWU1ZmYzMDkiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJuYmYiOjE2MzU2NzI5OTcsInNjb3BlIjpbIm1lc3NhZ2UucmVhZCIsIm1lc3NhZ2Uud3JpdGUiXSwiaXNzIjoiaHR0cDpcL1wvMTkyLjE2OC4xMzcuMTM5OjkwMDAiLCJleHAiOjE2MzU2NzMyOTcsImlhdCI6MTYzNTY3Mjk5N30.Et49jbPDeV5YiAvhDXhzFfuSMHLhxM41muIFS8-I-DDo3fiOs_IAdtoa_DNL0TNuBa16NhfJ86kqJCTuJ19bkJ7yoniua_1ZcHLsg2n9a4SDF5gMGstrc1y_DZNhWlfPlyP4IuZ7cbz86cg5W0hs7QxrVTX4xPFcpEpzb3EPSPuP9-E8qhtsyy29Lcz1vB2uwCoYExMeg0NIBurflyDAMFZEF3_X56-1z5Nn2bbtHaSfiVv0bS71NnT_yAY8npgqzcqsPfgfOyfMOaNgwt_dC0Z2IrQMjLJK8ASw0nokDkP7-wgi0u_HJhxk3IeQLi4MvXyHBBd6-dmY6zpQhSAuBw','2021-10-31 17:36:37','2021-10-31 17:41:36','message.read,message.write',_binary 'VL_bZWTtJxSWM3T6ccTE5guP0X-O9XVzCvDwmmNKLCdW5iPuyfTD3YDojHNLp8S7CqUlFyJrZNOcvoOtxWJHPhBS7Lhirv-Yk-zAqQYfe-Tbz9H4_h0tEiL5MJM3DFOF','2021-10-31 17:36:37','2021-10-31 09:36:37'),('messaging-client-oidc','user1','Bearer',_binary 'eyJraWQiOiIxNmM0MjM3Ni04Y2U2LTQwMmItOTY4MS04NDc1Y2MwN2Y5YjEiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJuYmYiOjE2MzU5MDQ5NzAsInNjb3BlIjpbIm9wZW5pZCJdLCJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEzNy4xMzk6OTAwMCIsImV4cCI6MTYzNTkwNTI3MCwiaWF0IjoxNjM1OTA0OTcwfQ.HrDLULT_mu5e9rtsAlLZMiARda_uSRfyp8j5A7BthVqQhF-GzKI-4jDB2XROJu8Mfiin-8vEt1ew6Y1oY5ugxt-lGFa486zNa3ApnKVVtdVmts1nREPB44vaRTB1xzGgsepxpHjON6i9qZc2dcWDdt-tmrEaU0B3uH3ofi4ERp1BeWfhPe_OWxUNKKvRC3SGp0uOFZVb3wO7VYOxRXGNjN_Gin4QzKcRUBUnnHmx3PHzvhSsOfBKyCOUUwbyI6V2DjY5l9qeYRt5jPTIOv-DIdVSL3pSk7E3vOIkkleFECdwolUcU9tR4ld2u_thu7S33Otao7w4xkT-uhUxt-XfFw','2021-11-03 10:02:50','2021-11-03 10:07:49','openid',_binary 'YglguOG4YBm6sA5JbQoiZMp3prvGxKuuBnoa4R9QOU2RYZP7ybAZWoiXYqfhGR5yB5sbLkT9lgjGzrPnBWEnf6jlYG8PfFHhH9_Rv-Wav0tMm-q3e8VluMAR4eJJsZyP','2021-11-03 10:02:50','2021-10-22 11:10:30');
/*!40000 ALTER TABLE `oauth2_authorized_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_account`
--

DROP TABLE IF EXISTS `sys_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_account` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id，账号id',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `creator` bigint unsigned NOT NULL DEFAULT '1' COMMENT '创建人账号id',
  `crete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_by` bigint unsigned DEFAULT NULL COMMENT '修改人账号id',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：1，删除。0，未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='账号表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_account`
--

LOCK TABLES `sys_account` WRITE;
/*!40000 ALTER TABLE `sys_account` DISABLE KEYS */;
INSERT INTO `sys_account` VALUES (1,'zhangsan',NULL,'$2a$10$HDsU3e9HJ3Yz9Uxm.ycegeDETxbY1jGmUwTIt45P7xLOxfpuRV/.6',1,'2021-05-24 23:31:38',NULL,'2021-11-09 08:11:43',0),(2,'admin',NULL,'$2a$10$ZVWbIRFYqzfri4w.ASKgzObodeOQREvyAwBQxz5fkqvnwEq3Btvhi',1,'2021-05-24 23:33:56',NULL,'2021-11-03 08:38:11',0),(3,'zhansan',NULL,'$2a$10$.kQgJsg82tAmob9dEn00UuG9//5Pty7YK8ZoMzrgzxMcKtcJmsgTq',1,'2021-05-24 23:31:38',NULL,NULL,0),(4,'zhansan1',NULL,'$2a$10$.kQgJsg82tAmob9dEn00UuG9//5Pty7YK8ZoMzrgzxMcKtcJmsgTq',1,'2021-05-24 23:31:38',NULL,'2021-11-09 08:03:18',0);
/*!40000 ALTER TABLE `sys_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_permission`
--

DROP TABLE IF EXISTS `sys_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_permission` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `pattern` varchar(200) COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口pattern',
  `http_method` varchar(44) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'http请求方式\n(GET,HEAD,POST,PUT,PATCH,DELETE,OPTIONS,TRACE)',
  `name` varchar(20) COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `description` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限描叙',
  `creator` bigint unsigned NOT NULL DEFAULT '1' COMMENT '创建人账号id',
  `crete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_by` bigint unsigned DEFAULT NULL COMMENT '修改人账号id',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：1，删除。0，未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_permission`
--

LOCK TABLES `sys_permission` WRITE;
/*!40000 ALTER TABLE `sys_permission` DISABLE KEYS */;
INSERT INTO `sys_permission` VALUES (1,'admin','GET','管理原权限',NULL,1,'2021-05-24 23:34:58',NULL,'2021-11-03 08:38:11',0),(2,'normal',NULL,'普通权限',NULL,1,'2021-05-24 23:34:58',NULL,'2021-11-03 08:38:11',0),(3,'/system/jdw-role-permission/role/role1',NULL,'动态接口权限',NULL,1,'2021-05-25 22:34:52',NULL,'2021-11-03 08:38:11',0),(4,'/',NULL,'首页接口',NULL,1,'2021-05-25 22:44:13',NULL,'2021-11-03 08:38:11',0),(5,'/user/insert',NULL,'用户新增接口',NULL,1,'2021-05-31 21:02:20',NULL,'2021-11-03 08:38:11',0),(6,'/user/update',NULL,'用户修改接口',NULL,1,'2021-05-31 21:02:32',NULL,'2021-11-03 08:38:11',0),(7,'/user/delete',NULL,'用户删除接口',NULL,1,'2021-05-31 21:03:03',NULL,'2021-11-03 08:38:11',0),(8,'/user/select ',NULL,'用户查询接口',NULL,1,'2021-05-31 21:03:14',NULL,'2021-11-03 08:38:11',0);
/*!40000 ALTER TABLE `sys_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` bigint unsigned DEFAULT NULL COMMENT '父角色id',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `description` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色描叙',
  `creator` bigint unsigned NOT NULL DEFAULT '1' COMMENT '创建人账号id',
  `crete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_by` bigint unsigned DEFAULT NULL COMMENT '修改人账号id',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：1，删除。0，未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,NULL,'admin','管理员','管理员',1,'2021-05-24 23:32:22',NULL,'2021-11-06 01:33:09',0),(2,NULL,'simple','普通用户1','',1,'2021-05-24 23:32:41',NULL,'2021-11-06 01:33:09',0),(3,1,'boss','老大','领头任务',1,'2021-11-02 11:19:25',NULL,'2021-11-06 02:08:27',0),(4,1,'boss','老大','领头任务',1,'2021-11-02 11:19:39',NULL,'2021-11-06 02:08:27',0),(5,2,'boss','老大','领头任务',1,'2021-11-03 03:03:23',NULL,'2021-11-06 02:08:27',0);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_permission`
--

DROP TABLE IF EXISTS `sys_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_permission` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint unsigned NOT NULL COMMENT '角色id',
  `permission_id` bigint unsigned NOT NULL COMMENT '权限id',
  `creator` bigint unsigned NOT NULL DEFAULT '1' COMMENT '创建人账号id',
  `crete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_by` bigint unsigned DEFAULT NULL COMMENT '修改人账号id',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：1，删除。0，未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色权限关系表\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_permission`
--

LOCK TABLES `sys_role_permission` WRITE;
/*!40000 ALTER TABLE `sys_role_permission` DISABLE KEYS */;
INSERT INTO `sys_role_permission` VALUES (1,1,1,1,'2021-05-24 23:35:40',NULL,'2021-11-03 08:38:11',0),(2,2,2,1,'2021-05-24 23:35:50',NULL,'2021-11-03 08:38:11',0),(3,1,3,1,'2021-05-25 22:43:08',NULL,'2021-11-03 08:38:11',0),(4,1,2,1,'2021-05-25 22:43:35',NULL,'2021-11-03 08:38:11',0),(5,1,4,1,'2021-05-25 22:43:44',NULL,'2021-11-03 08:38:11',0),(6,1,5,1,'2021-05-31 21:03:33',NULL,'2021-11-03 08:38:11',0),(7,1,6,1,'2021-05-31 21:03:36',NULL,'2021-11-03 08:38:11',0),(8,2,7,1,'2021-05-31 21:03:42',NULL,'2021-11-03 08:38:11',0),(9,2,8,1,'2021-05-31 21:03:51',NULL,'2021-11-03 08:38:11',0);
/*!40000 ALTER TABLE `sys_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `account_id` bigint unsigned DEFAULT NULL COMMENT '账号id',
  `nick_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
  `age` tinyint unsigned DEFAULT NULL COMMENT '年龄',
  `creator` bigint unsigned NOT NULL DEFAULT '1' COMMENT '创建人账号id',
  `crete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_by` bigint unsigned DEFAULT NULL COMMENT '修改人账号id',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：1，删除。0，未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,1,'张三12',14,1,'2021-05-24 23:55:41',NULL,'2021-11-03 08:38:12',0),(2,2,'管理12',200,1,'2021-05-24 23:55:47',NULL,'2021-11-03 08:38:12',0);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL COMMENT '用户id',
  `role_id` bigint unsigned NOT NULL COMMENT '角色id',
  `creator` bigint unsigned NOT NULL DEFAULT '1' COMMENT '创建人账号id',
  `crete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_by` bigint unsigned DEFAULT NULL COMMENT '修改人账号id',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：1，删除。0，未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户角色关系表\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1,2,1,'2021-05-24 23:34:13',NULL,'2021-11-03 08:38:12',0),(2,2,1,11,'2021-05-24 23:34:22',NULL,'2021-11-03 08:38:12',0);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-09 20:01:32
