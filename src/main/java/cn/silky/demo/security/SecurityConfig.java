package cn.silky.demo.security;

import cn.silky.demo.security.authorization.CustomSecuritySuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * @author ListJiang
 * @since 2021/11/6
 * description security配置
 */
@EnableWebFluxSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    @Bean
    SecurityWebFilterChain webHttpSecurity(ServerHttpSecurity http) {
        http
                .authorizeExchange((exchanges) -> exchanges
                        // 忘记密码则关闭验证，调用 /sys-account/reset-password/{id} 重置密码
                        // .anyExchange().authenticated()
                        .anyExchange().permitAll()
                )
                // 添加认证过滤器
                // .addFilterBefore(new CustomSecurityAuthorizationFilter(), SecurityWebFiltersOrder.LOGOUT)
                // 添加授权过滤器
                // .addFilterBefore(null, SecurityWebFiltersOrder.LOGOUT)
                // 添加自定义认证成功处理器
                .formLogin(formLoginSpec -> formLoginSpec.authenticationSuccessHandler(new CustomSecuritySuccessHandler()));
        http.csrf().disable().cors().disable();
        return http.build();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}