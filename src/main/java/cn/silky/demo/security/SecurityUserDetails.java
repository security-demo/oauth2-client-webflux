package cn.silky.demo.security;

import cn.silky.demo.web.entity.SysAccount;
import cn.silky.demo.web.entity.SysUser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author ListJiang
 * @since 2021/11/6
 * description 自定义UsearDetails
 */
@Getter
@Setter
@RequiredArgsConstructor
@Accessors(chain = true)
public class SecurityUserDetails implements UserDetails, Serializable {
    private static final long serialVersionUID = 1L;

    private final SysAccount sysAccount;
    private final SysUser sysUser;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return sysUser.getAuthorities();
    }

    @Override
    public String getPassword() {
        return sysAccount.getPassword();
    }

    @Override
    public String getUsername() {
        return sysAccount.getAccount();
    }

    @Override
    public boolean isAccountNonExpired() {
        // 可通过redis自定义实现临时账号
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // 可redis自定义实现多次登录失败短时锁定账号
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // 可redis自定义实现用户临时密码
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !sysAccount.isDeleted();
    }
}