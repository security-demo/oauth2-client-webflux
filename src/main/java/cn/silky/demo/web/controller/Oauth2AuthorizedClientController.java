package cn.silky.demo.web.controller;

import cn.silky.demo.web.service.IOauth2AuthorizedClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/oauth2-authorized-client")
public class Oauth2AuthorizedClientController {
    private IOauth2AuthorizedClientService iOauth2AuthorizedClientService;

}
