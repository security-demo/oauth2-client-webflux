package cn.silky.demo.web.controller;

import cn.silky.demo.web.entity.SysPermission;
import cn.silky.demo.web.repository.SysPermissionRepository;
import cn.silky.demo.web.service.ISysPermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.Arrays;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/sys-permission")
public class SysPermissionController {
    private final SysPermissionRepository sysPermissionRepository;
    private final ISysPermissionService iSysPermissionService;

    @GetMapping("/findSysPermissionByCreator/{creator}")
    Flux<SysPermission> findSysPermissionByCreator(@PathVariable Long creator) {
        return sysPermissionRepository.findSysPermissionByCreator(creator);
    }

    @GetMapping("/findSysPermissionByName/{name}")
    Flux<SysPermission> findSysPermissionByName(@PathVariable String name) {
        return sysPermissionRepository.findSysPermissionByName(name);
    }

    @GetMapping("/findSysPermissionByRoleIds/{roleIds}")
    Flux<SysPermission> findSysPermissionByRoleIds(@PathVariable String roleIds) {
        return sysPermissionRepository.findSysPermissionByRoleIds(Arrays.stream(roleIds.split(","))
                .map(Long::valueOf).toList());
    }


}