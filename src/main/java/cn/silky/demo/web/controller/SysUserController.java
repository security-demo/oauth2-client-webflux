package cn.silky.demo.web.controller;

import cn.silky.demo.web.entity.SysUser;
import cn.silky.demo.web.repository.SysUserRepository;
import cn.silky.demo.web.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/sys-user")
public class SysUserController {
    @Autowired
    private final ISysUserService iSysUserService;
    private final SysUserRepository sysUserRepository;

    @GetMapping(value = "/", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Flux<SysUser> all() {
        if (log.isDebugEnabled())
            log.debug("用户全量查询");
        return sysUserRepository.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Mono<SysUser> findById(@PathVariable Long id) {
        return sysUserRepository.findById(id);
    }

    @GetMapping(value = "/nickName")
    Mono<SysUser> findSysUserByNickNameEquals(String nickName){
        return sysUserRepository.findSysUserByNickNameEquals(nickName);
    }
}