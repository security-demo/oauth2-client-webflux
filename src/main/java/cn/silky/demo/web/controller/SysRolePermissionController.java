package cn.silky.demo.web.controller;

import cn.silky.demo.web.service.ISysRolePermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色权限关系表	 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/sys-role-permission")
public class SysRolePermissionController {
    private ISysRolePermissionService iSysRolePermissionService;

}