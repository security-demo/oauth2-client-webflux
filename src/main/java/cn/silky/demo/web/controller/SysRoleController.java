package cn.silky.demo.web.controller;

import cn.silky.demo.web.entity.SysRole;
import cn.silky.demo.web.repository.SysRoleRepository;
import cn.silky.demo.web.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/sys-role")
public class SysRoleController {
    private final ISysRoleService iSysRoleService;
    private final SysRoleRepository sysRoleRepository;

    @GetMapping(value = "/{id}")
    Mono<SysRole> findById(@PathVariable Long id) {
        return iSysRoleService.findById(id);
    }
}