package cn.silky.demo.web;

import cn.silky.demo.web.entity.SysUser;
import cn.silky.demo.web.repository.SysUserRepository;
import cn.silky.demo.web.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ListJiang
 * @since 2021/10/25
 * description
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("user")
public class UserController {

    private final ISysUserService iSysUserService;
    private final SysUserRepository sysUserRepository;

    @GetMapping(value = "/", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Flux<SysUser> all() {
        return sysUserRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    Mono<SysUser> findById(@PathVariable Long id) {
        return iSysUserService.findById(id);
    }

}