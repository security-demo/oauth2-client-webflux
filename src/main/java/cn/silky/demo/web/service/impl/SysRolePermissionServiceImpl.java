package cn.silky.demo.web.service.impl;

import cn.silky.demo.web.repository.SysRolePermissionRepository;
import cn.silky.demo.web.service.ISysRolePermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关系表	 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysRolePermissionServiceImpl implements ISysRolePermissionService {
    private final SysRolePermissionRepository sysRolePermissionRepository;

}