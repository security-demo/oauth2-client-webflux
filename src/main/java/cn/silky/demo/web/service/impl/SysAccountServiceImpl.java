package cn.silky.demo.web.service.impl;

import cn.silky.demo.security.SecurityUserDetails;
import cn.silky.demo.web.entity.SysAccount;
import cn.silky.demo.web.repository.SysAccountRepository;
import cn.silky.demo.web.service.ISysAccountService;
import cn.silky.demo.web.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.ReactiveUserDetailsPasswordService;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * 账号表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysAccountServiceImpl implements ISysAccountService {
    private final SysAccountRepository sysAccountRepository;
    private final ISysUserService iSysUserService;

    @Override
    public Mono<SysAccount> findById(Long id) {
        return sysAccountRepository.findById(id).flatMap(sysAccount ->
                iSysUserService.findSysUsersByAccountId(sysAccount::getId).map(sysAccount::setSysUser)
        );
    }

    @Override
    public Mono<SysAccount> findByAccount(String account) {
        return sysAccountRepository.findSysAccountByAccount(account).flatMap(sysAccount ->
                iSysUserService.findSysUsersByAccountId(sysAccount::getId).map(sysAccount::setSysUser)
        );
    }

    @Override
    public Mono<SysAccount> findByEmail(String email) {
        return sysAccountRepository.findSysAccountByEmail(email).flatMap(sysAccount ->
                iSysUserService.findSysUsersByAccountId(sysAccount::getId).map(sysAccount::setSysUser)
        );
    }

}