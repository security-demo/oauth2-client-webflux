package cn.silky.demo.web.service;

import cn.silky.demo.web.entity.SysAccount;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 账号表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface ISysAccountService {
    /**
     * @return 指定id账号
     */
    Mono<SysAccount> findById(Long id);

    /**
     * 通过账号查询账号实体
     *
     * @param account 账号
     * @return
     */
    Mono<SysAccount> findByAccount(String account);

    /**
     * 通过邮箱查询账号实体
     *
     * @param email 邮箱
     * @return
     */
    Mono<SysAccount> findByEmail(String email);
}