package cn.silky.demo.web.service.impl;

import cn.silky.demo.web.repository.SysUserRoleRepository;
import cn.silky.demo.web.service.ISysUserRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表	 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysUserRoleServiceImpl implements ISysUserRoleService {
    private final SysUserRoleRepository sysUserRoleRepository;

}