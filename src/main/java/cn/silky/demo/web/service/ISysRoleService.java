package cn.silky.demo.web.service;

import cn.silky.demo.web.entity.SysRole;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface ISysRoleService {

    Mono<SysRole> findById(Long id);

    Flux<SysRole> findSysRoleByUserId(Long userId);
}