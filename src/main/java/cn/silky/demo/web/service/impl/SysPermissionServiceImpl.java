package cn.silky.demo.web.service.impl;

import cn.silky.demo.web.repository.SysPermissionRepository;
import cn.silky.demo.web.service.ISysPermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysPermissionServiceImpl implements ISysPermissionService {
    private final SysPermissionRepository sysPermissionRepository;

}