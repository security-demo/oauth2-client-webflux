package cn.silky.demo.web.service.impl;

import cn.silky.demo.web.entity.SysUser;
import cn.silky.demo.web.repository.SysUserRepository;
import cn.silky.demo.web.service.ISysRoleService;
import cn.silky.demo.web.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.function.Supplier;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl implements ISysUserService {
    private final SysUserRepository sysUserRepository;
    private final ISysRoleService iSysRoleService;

    @Override
    public Mono<SysUser> findSysUsersByAccountId(Supplier<Long> supplier) {
        return sysUserRepository.findSysUsersByAccountId(supplier.get()).flatMap(sysUser ->
                iSysRoleService.findSysRoleByUserId(sysUser.getId()).collectList().map(sysRoles ->
                        sysUser.setSysRoles(sysRoles)));
    }

    @Override
    public Mono<SysUser> findById(Long id) {
        return sysUserRepository.findById(id).flatMap(sysUser ->
                iSysRoleService.findSysRoleByUserId(sysUser.getId()).collectList().map(sysRoles ->
                        sysUser.setSysRoles(sysRoles)));
    }
}