package cn.silky.demo.web.service;

import cn.silky.demo.web.entity.SysUser;
import reactor.core.publisher.Mono;

import java.util.function.Supplier;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface ISysUserService {
    Mono<SysUser> findSysUsersByAccountId(Supplier<Long> supplier);

    Mono<SysUser> findById(Long id);
}