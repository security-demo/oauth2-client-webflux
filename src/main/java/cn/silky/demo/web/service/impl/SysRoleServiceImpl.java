package cn.silky.demo.web.service.impl;

import cn.silky.demo.web.entity.SysRole;
import cn.silky.demo.web.repository.SysPermissionRepository;
import cn.silky.demo.web.repository.SysRoleRepository;
import cn.silky.demo.web.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.ReactiveUserDetailsPasswordService;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl implements ISysRoleService {
    private final SysRoleRepository sysRoleRepository;
    private final SysPermissionRepository sysPermissionRepository;

    @Override
    public Mono<SysRole> findById(Long id) {
        return sysRoleRepository.findById(id).flatMap(sysRole ->
                sysPermissionRepository.findSysPermissionByRoleId(sysRole.getId()).collectList()
                        .map(sysPermissions -> sysRole.setSysPermissions(sysPermissions)));

    }

    @Override
    public Flux<SysRole> findSysRoleByUserId(Long userId) {
        return sysRoleRepository.findSysRoleByUserId(userId).flatMap(sysRole ->
                sysPermissionRepository.findSysPermissionByRoleId(sysRole.getId()).collectList()
                        .map(sysPermissions -> sysRole.setSysPermissions(sysPermissions)));
    }


}