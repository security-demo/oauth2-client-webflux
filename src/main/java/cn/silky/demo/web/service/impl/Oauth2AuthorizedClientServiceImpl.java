package cn.silky.demo.web.service.impl;

import cn.silky.demo.web.repository.Oauth2AuthorizedClientRepository;
import cn.silky.demo.web.service.IOauth2AuthorizedClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class Oauth2AuthorizedClientServiceImpl implements IOauth2AuthorizedClientService {
    private final Oauth2AuthorizedClientRepository oauth2AuthorizedClientRepository;

}