package cn.silky.demo.web.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;


import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Getter
@Setter
@Accessors(chain = true)
@Table("oauth2_authorized_client")
public class Oauth2AuthorizedClient implements Serializable {

    private static final long serialVersionUID = 1L;

    private String clientRegistrationId;

    private String principalName;

    private String accessTokenType;

    private Blob accessTokenValue;

    private LocalDateTime accessTokenIssuedAt;

    private LocalDateTime accessTokenExpiresAt;

    private String accessTokenScopes;

    private Blob refreshTokenValue;

    private LocalDateTime refreshTokenIssuedAt;

    private LocalDateTime createdAt;


}