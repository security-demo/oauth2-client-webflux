package cn.silky.demo.web.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 角色表
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Getter
@Setter
@Accessors(chain = true)
@Table("sys_role")
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @Id
    private Long id;

    /**
     * 角色编码
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描叙
     */
    private String description;

    /**
     * 创建人账号id
     */
    private Long creator;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime creteTime;

    /**
     * 修改人账号id
     */
    private Long modifiedBy;

    /**
     * 逻辑删除标识：1，删除。0，未删除
     */
    private Byte deleted;

    public boolean isDeleted() {
        return deleted != null && deleted != 0;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = (byte) (deleted ? 1 : 0);
    }

    /**
     * 权限
     */
    @Transient
    private List<SysPermission> sysPermissions;


}