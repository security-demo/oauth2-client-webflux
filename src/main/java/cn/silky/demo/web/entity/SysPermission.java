package cn.silky.demo.web.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 权限表
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Getter
@Setter
@Accessors(chain = true)
@Table("sys_permission")
public class SysPermission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @Id
    private Long id;

    /**
     * 接口pattern
     */
    private String pattern;

    /**
     * http请求方式，以英文逗号拼接
     * 取值(GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE)
     */
    private String httpMethod;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限描叙
     */
    private String description;

    /**
     * 创建人账号id
     */
    private Long creator;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime creteTime;

    /**
     * 修改人账号id
     */
    private Long modifiedBy;

    /**
     * 逻辑删除标识：1，删除。0，未删除
     */
    private Byte deleted;

    public boolean isDeleted() {
        return deleted != null && deleted != 0;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = (byte) (deleted ? 1 : 0);
    }


}