package cn.silky.demo.web.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 用户角色关系表	
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
@Getter
@Setter
@Accessors(chain = true)
@Table("sys_user_role")
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 创建人账号id
     */
    private Long creator;

    /**
     * 创建时间
     */
    private LocalDateTime creteTime;

    /**
     * 修改人账号id
     */
    private Long modifiedBy;

    /**
     * 逻辑删除标识：1，删除。0，未删除
     */
    private Byte deleted;

    public boolean isDeleted() {
        return deleted != null && deleted != 0;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = (byte) (deleted ? 1 : 0);
    }


}