package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.SysRolePermission;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * <p>
 * 角色权限关系表	 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface SysRolePermissionRepository extends ReactiveCrudRepository<SysRolePermission, Long> {

}
