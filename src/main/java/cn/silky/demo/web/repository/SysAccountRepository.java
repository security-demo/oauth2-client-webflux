package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.SysAccount;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 账号表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface SysAccountRepository extends ReactiveCrudRepository<SysAccount, Long> {

    Mono<SysAccount> findSysAccountByAccount(String account);

    Mono<SysAccount> findSysAccountByEmail(String email);
}