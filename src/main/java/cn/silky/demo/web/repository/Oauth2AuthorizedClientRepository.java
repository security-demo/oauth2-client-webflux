package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.Oauth2AuthorizedClient;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface Oauth2AuthorizedClientRepository extends ReactiveCrudRepository<Oauth2AuthorizedClient, Long> {

}
