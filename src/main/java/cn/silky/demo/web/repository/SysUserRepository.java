package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.SysUser;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface SysUserRepository extends ReactiveCrudRepository<SysUser, Long> {
    @Query("""
            select * from sys_user where account_id = :accountId
            """)
    Mono<SysUser> findSysUsersByAccountId(Long accountId);

    Mono<SysUser> findSysUserByNickNameEquals(@Param("nickName") String nickName);

}