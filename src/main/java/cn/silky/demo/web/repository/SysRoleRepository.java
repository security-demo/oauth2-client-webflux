package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.SysRole;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface SysRoleRepository extends ReactiveCrudRepository<SysRole, Long> {
    @Query("""
            SELECT
                  	sr.*
            FROM
              sys_user su,
              sys_user_role sur,
              sys_role sr
            WHERE
              su.id = sur.user_id
              AND sur.role_id = sr.id
              AND su.id = :userId 
            """)
    Flux<SysRole> findSysRoleByUserId(Long userId);
}