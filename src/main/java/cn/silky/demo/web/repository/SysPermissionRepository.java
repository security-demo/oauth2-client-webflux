package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.SysPermission;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface SysPermissionRepository extends ReactiveCrudRepository<SysPermission, Long> {

    Flux<SysPermission> findSysPermissionByCreator(Long creator);

    // 由于该方法名符合标准单表查询方法，故无需写sql
    Flux<SysPermission> findSysPermissionByName(String name);

    @Query("""
            SELECT
            	sp.*
            FROM
            	sys_role_permission srp,
            	sys_permission sp
            WHERE
            	srp.permission_id = sp.id
            	AND srp.role_id = :roleId
            """)
    Flux<SysPermission> findSysPermissionByRoleId(Long roleId);

    @Query("""
            SELECT
            	sp.*
            FROM
            	sys_role_permission srp,
            	sys_permission sp
            WHERE
            	srp.permission_id = sp.id
            	AND srp.role_id in (:roleIds) 
            """)
    Flux<SysPermission> findSysPermissionByRoleIds(List<Long> roleIds);

}