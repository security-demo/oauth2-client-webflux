package cn.silky.demo.web.repository;

import cn.silky.demo.web.entity.SysUserRole;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * <p>
 * 用户角色关系表	 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-11-01
 */
public interface SysUserRoleRepository extends ReactiveCrudRepository<SysUserRole, Long> {

}
