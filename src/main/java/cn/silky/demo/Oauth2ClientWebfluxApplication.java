package cn.silky.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.validation.DataBinder;
import org.springframework.web.context.WebApplicationContext;

@SpringBootApplication
@EnableR2dbcRepositories
public class Oauth2ClientWebfluxApplication {

    public static void main(String[] args) {
        Resource resource=  null;
        UrlResource context=  null;
        ApplicationContext context1=  null;
        DataBinder context2=  null;
        Integer integer = Integer.valueOf(1);
        SpringApplication.run(Oauth2ClientWebfluxApplication.class, args);
    }

}