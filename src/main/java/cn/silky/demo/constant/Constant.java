package cn.silky.demo.constant;

import java.time.Duration;

/**
 * @author ListJiang
 * @since 2021/10/21
 * description
 */
public interface Constant {
    String TOKEN = "Authorization";
    String DEFAULT_PASSWORD = "password";
    String TOKEN_REDIS_KEY = "token-key:";
    String rolePrefix = "ROLE_";
    Duration TOKEN_TIMEOUT = Duration.ofMinutes(30);
}