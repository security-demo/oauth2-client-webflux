package ${package.Service};

import ${package.Entity}.${entity};

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName}
<#else>
public interface ${table.serviceName} {

    Mono<${entity}> save(${entity} entity);

    Flux<${entity}> saveAll(Iterable<${entity}> entities);

    Flux<${entity}> saveAll(Publisher<${entity}> entityStream);

    Mono<${entity}> findById(ID id);

    Mono<${entity}> findById(Publisher<Long> id);

    Mono<Boolean> existsById(ID id);

    Mono<Boolean> existsById(Publisher<Long> id);

    Flux<${entity}> findAll();

    Flux<${entity}> findAllById(Iterable<Long> ids);

    Flux<${entity}> findAllById(Publisher<Long> idStream);

    Mono<Long> count();

    Mono<Void> deleteById(ID id);

    Mono<Void> deleteById(Publisher<Long> id);

    Mono<Void> delete(T entity);

    Mono<Void> deleteAllById(Iterable<Long> ids);

    Mono<Void> deleteAll(Iterable<? extends ${entity}> entities);

    Mono<Void> deleteAll(Publisher<? extends ${entity}> entityStream);

    Mono<Void> deleteAll();
}
</#if>