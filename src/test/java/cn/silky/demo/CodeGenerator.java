package cn.silky.demo;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Author ListJiang
 * @Since class mybatis-plus代码生成器
 * datetime 2021/7/1 14:30
 */
public class CodeGenerator {

    private static String projectPath = "D:\\ideaProject\\gitee\\security-demo\\oauth2-client-webflux\\src\\main\\java";
    private static String author = "ListJiang";
    private static String packageParent = "cn.silky.demo.web";
    private static String url = "jdbc:mysql://192.168.137.139:3306/sso-client?serverTimezone=GMT%2b8" +
            "&zeroDateTimeBehaviorconvertToNull&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true";
    private static String driverName = "com.mysql.cj.jdbc.Driver";
    private static String user = "root";
    private static String password = "root";

    // 处理 all 情况
    protected static List<String> getTables(String tables) {
        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }

    public static void main(String[] args) {
        FastAutoGenerator.create(url, user, password)
                // 全局配置
                .globalConfig((scanner, builder) ->
                        builder.author(author)
                                .fileOverride()
                                .outputDir(projectPath))
                // 包配置
                .packageConfig((scanner, builder) -> builder.parent(packageParent)
                        .mapper("repository"))
                // 策略配置
                .strategyConfig((scanner, builder) -> builder.addInclude(getTables(scanner.apply("请输入表名，多个英文逗号分隔？所有输入" +
                                " all")))
                        .entityBuilder().enableLombok().enableChainModel()
                        .mapperBuilder().formatMapperFileName("%sRepository").superClass(ReactiveCrudRepository.class)
                        .serviceBuilder()
                        .controllerBuilder().enableRestStyle().enableHyphenStyle()
                        .build())
                // 自定义模板文件
                .templateConfig((scanner, builder) -> builder
                        .entity("/templates/entity.java")
                        .mapper("/templates/mapper.java")
                        .mapperXml("/templates/mapper.xml")
                        .service("/templates/service.java")
                        .serviceImpl("/templates/serviceImpl.java")
                        .controller("/templates/controller.java")
                        .build())
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}