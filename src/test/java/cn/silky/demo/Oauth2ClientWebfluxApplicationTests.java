package cn.silky.demo;

import freemarker.template.TemplateException;
import io.r2dbc.spi.ConnectionFactory;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = Oauth2ClientWebfluxApplication.class, webEnvironment =
        SpringBootTest.WebEnvironment.RANDOM_PORT)
class Oauth2ClientWebfluxApplicationTests {

    @Resource
    private ConnectionFactory connectionFactory;

    @Test
    void contextLoads() {
    }

    @Test
    public void test() {
        DatabaseClient client = DatabaseClient.create(connectionFactory);
        // .......
    }

    public String longestPalindrome(String s) {
        // 以左下标开始
        if (s.length() == 1) {
            return s;
        }
        char[] chars = s.toCharArray();
        String result = s.substring(0, 1);
        int a = 1;
        for (int i = 0; i < s.length() - 1; i++) {
            // abba式，i对应b
            // 中间开花，限制开花大小;
            // 开花最大半径为 r = s.length() / 2
            // j为开花下标偏移量(定点为0)
            // 当 i < r 时，j偏移量取值范围[0,i]
            // 当 i >= r 时，偏移量取值范围[0,abs(i-2*r)-1]
            if (chars[i] == chars[i + 1]) {
                int r = s.length() / 2 -1;
                int maxJ = (i < r ? i : 2 * r - i - 1);
                for (int j = 0; j <= maxJ; j++) {
                    if (chars[i - j] == chars[i + j + 1]) {
                        a = 2 * j + 2;
                        if (a > result.length()) {
                            result = s.substring(i - j, i + j + 2);
                        }
                        continue;
                    } else {
                        break;
                    }
                }
            }
            // aba式，i对应b
            // 中间开花，限制开花大小;
            // 0、1、2、3、4、5、6、7     3
            // 0、1、2、3、4、5、6、7、8  4
            // 开花最大半径为 r = Math.ceil(s.length() / 2) -1
            // j为开花下标偏移量(定点为0)
            // 当 i <= r 时，j偏移量取值范围[1,i]
            // 当 i > r 时，偏移量取值范围[1,s.length() - i -1]
            if (i > 0 && chars[i - 1] == chars[i + 1]) {
                int r = Double.valueOf(Math.ceil(s.length() / 2.0)).intValue() - 1;
                for (int j = 1; j <= (i < r ? i : s.length() - i - 2); j++) {
                    if (chars[i - j] == chars[i + j]) {
                        a = 2 * j + 1;
                        if (a > result.length()) {
                            result = s.substring(i - j, i + j + 1);
                        }
                        continue;
                    } else {
                        break;
                    }
                }
            }
        }
        return result;
    }

    @Test
    void requestMatcher() {
        String aa = longestPalindrome("aa");
        String ccc = longestPalindrome("ccc");
        String aaaa = longestPalindrome("aaaa");
        String babad = longestPalindrome("babad");
        System.out.println(babad);
    }

    @Test
    void match() throws NoSuchFieldException {
        class A{
            private String a;
            public String b;
            public String b1;
            protected String c;
        }
        class B extends A{
            private String a1;
            public String b1;
            protected String c1;

        }

        Field b = B.class.getField("b");
        Field[] fields1 = B.class.getFields();
        Field[] fields2 = B.class.getDeclaredFields();

        System.out.println("b");
    }

    @Test
    void pdf() throws TemplateException, IOException {
        Map<String, Object> dataModel = new HashMap<>();
        dataModel.put("author", "Saisimon");
        dataModel.put("test", "a");
        dataModel.put("url", "https://blog.csdn.net/sai_simon/article/details/98898380");
        // 解析 FreeMarker 模板
        String content = PDFUtil.parseFreemarker("template.html", dataModel);
        // 导出 PDF 文件路径
        String pdf = System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "freemarker.pdf";
        // 微软雅黑字体
        String font = System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "SIMLI.TTF";
        // 导出到 PDF 文件
        PDFUtil.exportPDF(content, pdf, font);
    }
}